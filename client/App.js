import React, { Component } from "react";
import braintree from "braintree-web-drop-in";
import Braintree from "braintree-dropin-react";
import Axios from "axios";

class App extends Component {
  state = {
    clientToken: null
  };

  componentDidMount = async () => {
    try {
      const { data } = await Axios({
        method: "get",
        url: "/api/payment/getClientToken"
      });
      this.setState({ clientToken: data.clientToken });
    } catch (err) {
      console.log(err);
    }
  };

  handlePaymentMethod = async payload => {
    try {
      const { data } = await Axios({
        method: "post",
        url: "/api/payment/checkout",
        data: { paymentMethodNonce: payload.nonce }
      });
      console.log(data.result);
    } catch (err) {
      console.log(err);
    }
  };

  onCreate = instance => {
    console.log("onCreate");
  };

  onDestroyStart = () => {
    console.log("onDestroyStart");
  };

  onDestroyEnd = () => {
    console.log("onDestroyEnd");
  };

  onError = error => {
    console.log("onError", error);
  };

  render() {
    const result = this.state.clientToken ? (
      <Braintree
        braintree={braintree}
        authorizationToken={this.state.clientToken}
        handlePaymentMethod={this.handlePaymentMethod}
        submitButtonText="Pay"
        onCreate={this.onCreate}
        onDestroyStart={this.onDestroyStart}
        onDestroyEnd={this.onDestroyEnd}
        onError={this.onError}
        card={{
          cardholderName: true,
          overrides: {
            fields: {
              cvv: {
                maskInput: true
              }
            }
          }
        }}
      />
    ) : (
      <div>
        <p>loading ...</p>
      </div>
    );
    return <div>{result}</div>;
  }
}

export default App;
