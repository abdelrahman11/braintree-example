const express = require("express");
const router = express.Router();
const braintree = require("braintree");

router.post("/checkout", function(req, res, next) {
  const gateway = braintree.connect({
    environment: braintree.Environment.Sandbox,
    // Use your own credentials from the sandbox Control Panel here
    merchantId: process.env.merchantId,
    publicKey: process.env.publicKey,
    privateKey: process.env.privateKey
  });

  // Use the payment method nonce here
  const { paymentMethodNonce } = req.body;
  console.log(paymentMethodNonce);
  // Create a new transaction for $10
  const newTransaction = gateway.transaction.sale(
    {
      amount: "10.00",
      paymentMethodNonce,
      options: {
        // This option requests the funds from the transaction
        // once it has been authorized successfully
        submitForSettlement: true
      }
    },
    (error, result) => {
      if (result) {
        res.status(200).json({ result });
      } else {
        res.status(500).json({ error });
      }
    }
  );
});

router.get("/getClientToken", (req, res) => {
  const gateway = braintree.connect({
    environment: braintree.Environment.Sandbox,
    // Use your own credentials from the sandbox Control Panel here
    merchantId: process.env.merchantId,
    publicKey: process.env.publicKey,
    privateKey: process.env.privateKey
  });

  gateway.clientToken.generate({}, function(err, response) {
    const clientToken = response.clientToken;
    if (!err) {
      res.status(200).json({ clientToken });
    } else {
      res.status(500).json({ error });
    }
  });
});

module.exports = router;
