const express = require("express");
const path = require("path");
const logger = require("morgan");
const payment = require("./routes/payment");

const app = express();
app.use(express.static(path.join(__dirname + "/client/dist")));
app.use(logger("dev"));
app.use(express.json());

app.use("/api/payment", payment);

app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname + "/client/dist/index.html"));
});

// error handler
app.use(function(err, req, res, next) {
  res.status(err.status || 500).json({ error: err.message });
});

module.exports = app;
